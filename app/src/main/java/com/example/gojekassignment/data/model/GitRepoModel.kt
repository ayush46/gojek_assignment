package com.example.gojekassignment.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GitRepoModel (
    @SerializedName("name") var name: String?,
    @SerializedName("owner") var owner: Owner?,
    @SerializedName("description") var description: String?,
    @SerializedName("language") var language: String?,
    @SerializedName("stargazers_count") var stars: Int?,
    @SerializedName("forks_count") var forks: Int?,
    @SerializedName("html_url") var url: String?) : Serializable

data class Owner(
    @SerializedName("avatar_url") var avatar: String?)