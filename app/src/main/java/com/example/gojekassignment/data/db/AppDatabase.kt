package com.example.gojekassignment.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.gojekassignment.data.db.dao.RepositoryDao
import com.example.gojekassignment.data.db.entity.Repository

@Database(entities = [Repository::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun repositoryDao() : RepositoryDao

    companion object{
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { appDatabase -> INSTANCE = appDatabase }
            }

        private fun buildDatabase(context: Context): AppDatabase = Room.databaseBuilder(context.applicationContext,
            AppDatabase::class.java, "repositoryDatabase.db").build()
    }
}