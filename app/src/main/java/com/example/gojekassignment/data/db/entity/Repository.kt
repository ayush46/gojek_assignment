package com.example.gojekassignment.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.gojekassignment.utils.Constant.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
class Repository (
    @PrimaryKey var repoName : String,
    @ColumnInfo(name = "description") var description : String?,
    @ColumnInfo(name = "image") var image : String?,
    @ColumnInfo(name = "repoUrl") var repoUrl : String?,
    @ColumnInfo(name = "language") var language : String?,
    @ColumnInfo(name = "star") var star : Int?,
    @ColumnInfo(name = "fork") var fork : Int?)