package com.example.gojekassignment.data.model

data class GitRepoUIModel (
    val repoName : String? = null,
    val description : String? = null,
    val image : String? = null,
    val repoUrl : String? = null,
    val language : String? = null,
    val stars : Int? = null,
    val forks : Int? = null,
    var isExpanded : Boolean = false)