package com.example.gojekassignment.data.db.dao

import androidx.room.*
import com.example.gojekassignment.data.db.entity.Repository
import com.example.gojekassignment.utils.Constant.Companion.TABLE_NAME

@Dao
interface RepositoryDao {

    @Query("SELECT * FROM $TABLE_NAME")
    fun getAll() : List<Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(repos : List<Repository>?)

    @Delete
    fun deleteAll(repos: List<Repository>?)
}