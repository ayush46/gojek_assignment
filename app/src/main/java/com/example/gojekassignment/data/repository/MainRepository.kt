package com.example.gojekassignment.data.repository

import com.example.gojekassignment.data.api.ApiHelper

class MainRepository(private val apiHelper: ApiHelper) {
     suspend fun getGitRepos() = apiHelper.getGitRepositories()
}