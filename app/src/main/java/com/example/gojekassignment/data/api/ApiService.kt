package com.example.gojekassignment.data.api

import com.example.gojekassignment.data.model.GitRepoModel
import retrofit2.http.GET

interface ApiService {
    @GET("repos")
    suspend fun getGitRepositories() : ArrayList<GitRepoModel>
}