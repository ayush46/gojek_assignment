package com.example.gojekassignment.data.api

class ApiHelper(private val apiService: ApiService) {
    suspend fun getGitRepositories() = apiService.getGitRepositories()
}