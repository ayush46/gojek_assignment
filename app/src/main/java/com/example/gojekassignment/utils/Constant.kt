package com.example.gojekassignment.utils

class Constant {
    companion object {
        const val SHARED_PREFERENCE = "com.example.gojekassignment"
        const val TABLE_NAME = "Repositories"
        const val DB_CACHE_KEY = "DB_CACHE"
        const val DB_CACHE_TIMEOUT = 1
    }

    enum class ClickEventName {
        EXPAND_EVENT
    }
}