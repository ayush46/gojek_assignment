package com.example.gojekassignment.utils.extention

import java.text.NumberFormat
import java.util.*

fun String.formatInCurrency() : String {
    val formatter = NumberFormat.getInstance(Locale("en", "IN"))
    return formatter.format(this.toFloat())
}