package com.example.gojekassignment.utils.enum

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}