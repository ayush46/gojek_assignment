package com.example.gojekassignment.utils.enum

enum class ErrorEnum(var str : String) {
    UNABLE_TO_FETCH_DATA("Something went wrong...")
}