package com.example.gojekassignment.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.example.gojekassignment.utils.Constant.Companion.SHARED_PREFERENCE
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class Util {
    companion object {
        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val network = connectivityManager.activeNetwork ?: return false
                val actNw = connectivityManager.getNetworkCapabilities(network) ?: return false
                return when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            } else {
                val nwInfo = connectivityManager.activeNetworkInfo ?: return false
                return nwInfo.isConnected
            }
        }

        private fun getSharedPreference(ctx: Context, key: String): Date? {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US)
            val pref = ctx.getSharedPreferences(SHARED_PREFERENCE, Context.MODE_PRIVATE)
            return if (pref?.contains(key)!!) sdf.parse(pref.getString(key, "")!!) else null
        }

        fun setSharedPreference(key: String, value: Date, context: Context) {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US)
            val pref = context.getSharedPreferences(SHARED_PREFERENCE, Context.MODE_PRIVATE)
            val editor = pref.edit()
            editor.putString(key, sdf.format(value))
            editor.apply()
        }

        private fun getDBCacheTimeOut(application: Application): Long {
            val lastUpdatedCacheTime = getSharedPreference(application, Constant.DB_CACHE_KEY)
            val currentTime = Calendar.getInstance().time

            if(lastUpdatedCacheTime != null) {
                val timeDiff = currentTime.time - lastUpdatedCacheTime.time
                return TimeUnit.MILLISECONDS.toMinutes(timeDiff)
            }
            return -1
        }

        fun isCacheExpire(application: Application): Boolean {
            val timeout = getDBCacheTimeOut(application)
            return (timeout == -1L || timeout >= Constant.DB_CACHE_TIMEOUT)
        }
    }
}