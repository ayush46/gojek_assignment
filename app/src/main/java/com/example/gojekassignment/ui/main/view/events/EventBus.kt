package com.example.gojekassignment.ui.main.view.events

import org.greenrobot.eventbus.EventBus

object EventBus : IEventBus {

    private val mEventBus: EventBus = EventBus.getDefault()

    override fun <T : Event?> postEvent(event: T) {
        mEventBus.post(event)
    }

    override fun register(subscriber: Any?) {
        if (!mEventBus.isRegistered(subscriber)) {
            mEventBus.register(subscriber)
        }
    }

    override fun unregister(subscriber: Any?) {
        if (mEventBus.isRegistered(subscriber)) {
            mEventBus.unregister(subscriber)
        }
    }
}