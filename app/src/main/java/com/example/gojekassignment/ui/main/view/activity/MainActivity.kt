package com.example.gojekassignment.ui.main.view.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.gojekassignment.R
import com.example.gojekassignment.data.api.ApiHelper
import com.example.gojekassignment.data.api.RetrofitBuilder
import com.example.gojekassignment.data.model.GitRepoUIModel
import com.example.gojekassignment.ui.base.ViewModelFactory
import com.example.gojekassignment.ui.main.callback.ExpandTupleCallback
import com.example.gojekassignment.ui.main.view.adapter.GitRepoAdapter
import com.example.gojekassignment.ui.main.view.events.EventBus
import com.example.gojekassignment.ui.main.view.events.ExpandEvent
import com.example.gojekassignment.ui.main.viewmodel.MainViewModel
import com.example.gojekassignment.utils.Util
import com.example.gojekassignment.utils.enum.Status
import org.greenrobot.eventbus.Subscribe

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener, ExpandTupleCallback {

    private lateinit var tvError: TextView
    private lateinit var errorView: View
    private lateinit var llRetry: LinearLayout
    private lateinit var progressbar: ProgressBar
    private lateinit var gitRepoAdapter: GitRepoAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var rvRepository: RecyclerView
    private lateinit var mViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()
        initViews()
        registerEventBus()
        getRepository()
    }

    private fun initViewModel() {
        mViewModel = ViewModelProvider(this, ViewModelFactory(ApiHelper(RetrofitBuilder.apiService), this.application))
            .get(MainViewModel::class.java)
    }

    private fun initViews() {
        rvRepository = findViewById(R.id.rv_repository)
        swipeRefreshLayout = findViewById(R.id.swipe_layout)
        progressbar = findViewById(R.id.progressBar)
        tvError = findViewById(R.id.tv_error_msg)
        errorView = findViewById(R.id.error_view)
        llRetry = findViewById(R.id.ll_retry)
        swipeRefreshLayout.setOnRefreshListener(this)

        val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        gitRepoAdapter = GitRepoAdapter()
        rvRepository.apply {
            layoutManager = manager
            hasFixedSize()
            adapter = gitRepoAdapter
        }

        llRetry.setOnClickListener {
            getRepository()
        }
    }

    private fun getRepository() {
        mViewModel.getGitRepository().observe(this, {
            it.let { resource ->
                when(resource.status) {
                    Status.SUCCESS -> {
                        handleViewVisibility()
                        resource.data?.let { repos -> populateUI(repos)}
                    }

                    Status.ERROR -> showErrorScreen(resource.message)

                    Status.LOADING -> progressbar.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun registerEventBus() {
        EventBus.register(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.sort_by_star -> mViewModel.sortRepoByStar()
            R.id.sort_by_name -> mViewModel.sortRepoByName()
        }
        mViewModel.uiData?.let { gitRepoAdapter.setData(it) }
        return super.onOptionsItemSelected(item)
    }

    private fun populateUI(gitRepoList: ArrayList<GitRepoUIModel>) {
        gitRepoAdapter.setData(gitRepoList)
    }

    @Subscribe
    override fun onTupleClicked(event: ExpandEvent) {
        gitRepoAdapter.updateAdapterView(event.uiData)
    }

    private fun showErrorScreen(errorMsg: String?) {
        errorView.visibility = View.VISIBLE
        swipeRefreshLayout.visibility = View.GONE
        progressbar.visibility = View.GONE
        tvError.text = errorMsg

        if(!Util.isNetworkAvailable(this)) Toast.makeText(this, R.string.no_iternet_connection, Toast.LENGTH_SHORT).show()
    }

    private fun handleViewVisibility() {
        progressbar.visibility = View.GONE
        swipeRefreshLayout.isRefreshing = false
        errorView.visibility = View.GONE
        swipeRefreshLayout.visibility = View.VISIBLE
    }

    private fun unRegisterEventBus() {
        EventBus.unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        unRegisterEventBus()
    }

    override fun onRefresh() {
        getRepository()
    }

}