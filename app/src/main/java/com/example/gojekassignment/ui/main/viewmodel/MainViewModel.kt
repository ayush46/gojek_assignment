package com.example.gojekassignment.ui.main.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.gojekassignment.data.db.dao.RepositoryDao
import com.example.gojekassignment.data.db.entity.Repository
import com.example.gojekassignment.data.model.GitRepoUIModel
import com.example.gojekassignment.data.repository.MainRepository
import com.example.gojekassignment.ui.main.mapper.DataMapper
import com.example.gojekassignment.utils.Constant.Companion.DB_CACHE_KEY
import com.example.gojekassignment.utils.Resource
import com.example.gojekassignment.utils.Util
import com.example.gojekassignment.utils.enum.ErrorEnum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class MainViewModel(private val mainRepository: MainRepository, private val application: Application,
                    private val repositoryDao: RepositoryDao) : ViewModel() {

    private var mapper: DataMapper = DataMapper()
    var uiData: ArrayList<GitRepoUIModel>? = null

    fun getGitRepository() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))

        try {
            if(!Util.isCacheExpire(application))
                uiData = mapper.convertDaoToUiModel(getRepositoriesFromDb())
            else {
                val data = mainRepository.getGitRepos()
                uiData = mapper.convertRawToUIModel(data)
                saveInCache()
            }
            emit(Resource.success(uiData))
        } catch (exception : Exception) {
            emit(Resource.error(data = null, message = exception.message ?: ErrorEnum.UNABLE_TO_FETCH_DATA.str))
        }
    }

    private fun saveInCache() {
        val daoData = mapper.convertToDao(uiData)
        Util.setSharedPreference(DB_CACHE_KEY, Calendar.getInstance().time, application)
        deleteRepositoriesFromDb(getRepositoriesFromDb())
        insertRepositoriesInDb(daoData)
    }

    fun sortRepoByStar() {
        uiData?.sortWith { uiData1, uiData2 -> uiData2.stars?.compareTo(uiData1.stars!!)!! }
    }

    fun sortRepoByName() {
        uiData?.sortWith { uiData1, uiData2 -> uiData1.repoName?.compareTo(uiData2.repoName!!)!! }
    }

    private fun insertRepositoriesInDb(daoData: ArrayList<Repository>?) {
        runBlocking(Dispatchers.Default){
            withContext(Dispatchers.Default) {
                repositoryDao.insert(daoData)
            }
            return@runBlocking
        }
    }

    private fun deleteRepositoriesFromDb(daoData: ArrayList<Repository>?) {
        runBlocking(Dispatchers.Default){
            withContext(Dispatchers.Default) {
                repositoryDao.deleteAll(daoData)
            }
            return@runBlocking
        }
    }

    private fun getRepositoriesFromDb() : ArrayList<Repository> = runBlocking(Dispatchers.Default) {
        val result = withContext(Dispatchers.Default) {
            repositoryDao.getAll()
        }
        return@runBlocking result as ArrayList<Repository>
    }

}