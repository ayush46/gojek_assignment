package com.example.gojekassignment.ui.main.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gojekassignment.R
import com.example.gojekassignment.data.model.GitRepoUIModel
import com.example.gojekassignment.ui.main.view.viewholder.GitRepoViewHolder

class GitRepoAdapter : RecyclerView.Adapter<GitRepoViewHolder>() {

    private var mRepoList : ArrayList<GitRepoUIModel>? = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GitRepoViewHolder {
        val mInflater = LayoutInflater.from(parent.context)
        val mView = mInflater.inflate(R.layout.row_item_layout, parent,false)
        return GitRepoViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return mRepoList?.size ?: 0
    }

    override fun onBindViewHolder(holder: GitRepoViewHolder, position: Int) {
        mRepoList?.get(position)?.let { holder.bindViewData(it) }
    }

    fun setData(list: ArrayList<GitRepoUIModel>) {
        mRepoList?.clear()
        mRepoList?.addAll(list)
        notifyDataSetChanged()
    }

    fun updateAdapterView(uiData: GitRepoUIModel) {
        mRepoList?.forEach { item ->
            if(uiData.repoName.equals(item.repoName, true)) uiData.isExpanded = true else item.isExpanded = false
            notifyDataSetChanged()
        }
    }

}