package com.example.gojekassignment.ui.main.callback

import com.example.gojekassignment.ui.main.view.events.ExpandEvent

interface ExpandTupleCallback {
    fun onTupleClicked(event : ExpandEvent)
}