package com.example.gojekassignment.ui.base

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.gojekassignment.data.api.ApiHelper
import com.example.gojekassignment.data.db.AppDatabase
import com.example.gojekassignment.data.repository.MainRepository
import com.example.gojekassignment.ui.main.viewmodel.MainViewModel

class ViewModelFactory(private val apiHelper: ApiHelper, private val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java))
            return MainViewModel(MainRepository(apiHelper), application, AppDatabase.getDatabase(application).repositoryDao()) as T
        throw IllegalArgumentException("Unknown class name")
    }
}