package com.example.gojekassignment.ui.main.mapper

import com.example.gojekassignment.data.db.entity.Repository
import com.example.gojekassignment.data.model.GitRepoModel
import com.example.gojekassignment.data.model.GitRepoUIModel

class DataMapper {

    fun convertRawToUIModel(rawData : ArrayList<GitRepoModel>) : ArrayList<GitRepoUIModel> {
        val gitRepoUIList = ArrayList<GitRepoUIModel>()

        rawData.forEach { data ->
            gitRepoUIList.add(
                GitRepoUIModel(data.name, data.description, data.owner?.avatar, data.url,
                    data.language, data.stars, data.forks)
            )
        }

        return gitRepoUIList
    }

    fun convertToDao(uiData: ArrayList<GitRepoUIModel>?): ArrayList<Repository> {
        val daoData : ArrayList<Repository> = ArrayList()
        uiData?.forEach { data ->
            daoData.add(Repository(data.repoName!!, data.description, data.image, data.repoUrl,
                data.language, data.stars, data.forks))
        }
        return daoData
    }

    fun convertDaoToUiModel(daoData: List<Repository>): ArrayList<GitRepoUIModel> {
        val uiRepoList : ArrayList<GitRepoUIModel> = ArrayList()
        daoData.forEach { data ->
            uiRepoList.add(GitRepoUIModel(data.repoName, data.description, data.image, data.repoUrl,
                data.language, data.star, data.fork))
        }
        return uiRepoList
    }
}