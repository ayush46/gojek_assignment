package com.example.gojekassignment.ui.main.view.events;

public interface IEventBus {

    <T extends Event> void postEvent(T event);

    void register(Object subscriber);

    void unregister(Object subscriber);
}
