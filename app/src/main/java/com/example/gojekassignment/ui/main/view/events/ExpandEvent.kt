package com.example.gojekassignment.ui.main.view.events

import com.example.gojekassignment.data.model.GitRepoUIModel
import com.example.gojekassignment.utils.Constant

class ExpandEvent(val uiData: GitRepoUIModel) : Event(Constant.ClickEventName.EXPAND_EVENT.name)