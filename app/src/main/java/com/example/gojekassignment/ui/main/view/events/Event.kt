package com.example.gojekassignment.ui.main.view.events

import android.util.Log

abstract class Event protected constructor(var name: String?) {

    fun post() {
        Log.d("Event", "Fired Event : $this")
        EventBus.postEvent(this)
    }
}