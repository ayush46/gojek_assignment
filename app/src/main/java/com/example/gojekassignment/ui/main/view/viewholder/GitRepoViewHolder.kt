package com.example.gojekassignment.ui.main.view.viewholder

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.gojekassignment.R
import com.example.gojekassignment.data.model.GitRepoUIModel
import com.example.gojekassignment.ui.main.view.events.ExpandEvent
import com.example.gojekassignment.utils.extention.formatInCurrency

class GitRepoViewHolder(view : View) : RecyclerView.ViewHolder(view) {

    private var mContext : Context
    private var rootView : ConstraintLayout
    private var ivLogo : ImageView? = null
    private var tvRepoName : TextView? = null
    private var tvDescription : TextView? = null
    private var tvRepoUrl : TextView? = null
    private var tvLanguage : TextView? = null
    private var tvStar : TextView? = null
    private var tvFork : TextView? = null

    init {
        mContext = view.context
        rootView = view.findViewById(R.id.root_view)
        ivLogo = view.findViewById(R.id.iv_logo)
        tvRepoName = view.findViewById(R.id.tv_repo)
        tvDescription = view.findViewById(R.id.tv_description)
        tvRepoUrl = view.findViewById(R.id.tv_repo_url)
        tvLanguage = view.findViewById(R.id.tv_language)
        tvStar = view.findViewById(R.id.tv_star)
        tvFork = view.findViewById(R.id.tv_fork)
    }

    fun bindViewData(uiData : GitRepoUIModel){

        ivLogo?.let {
            Glide.with(mContext).load(uiData.image)
                .into(it)
        }

        tvRepoName?.text = uiData.repoName
        tvDescription?.text = uiData.description
        tvRepoUrl?.text = uiData.repoUrl
        tvLanguage?.text = uiData.language
        tvStar?.text = uiData.stars.toString().formatInCurrency()
        tvFork?.text = uiData.forks.toString().formatInCurrency()

        if(uiData.isExpanded) expandCollapseView(View.VISIBLE) else expandCollapseView(View.GONE)

        rootView.setOnClickListener {
            val expandEvent = ExpandEvent(uiData)
            expandEvent.post()
        }

    }

    private fun expandCollapseView(visibility : Int) {
        tvRepoUrl?.visibility = visibility
        tvLanguage?.visibility = visibility
        tvStar?.visibility = visibility
        tvFork?.visibility = visibility
    }

}