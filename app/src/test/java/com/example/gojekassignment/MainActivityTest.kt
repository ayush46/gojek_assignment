package com.example.gojekassignment

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.gojekassignment.ui.main.view.activity.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.ViewAction as ViewAction1

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun test_isProgressBarVisible_onAppLaunch() {
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))
    }

    @Test
    fun test_isRecyclerViewVisible() {
        onView(withId(R.id.rv_repository)).check(matches(isDisplayed()))
    }

    @Test
    fun test_clickListItem_isItemExpanded() {
        onView(withId(R.id.rv_repository)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3,
            clickOnViewChild(R.id.tv_description)))

        onView(withId(R.id.tv_fork)).check(matches(withText("362")))
    }

    private fun clickOnViewChild(viewId: Int) = object : ViewAction1 {
        override fun getConstraints() = null

        override fun getDescription() = "Click on a child view with specified id."

        override fun perform(uiController: UiController, view: View)  {
            view.performClick()
            uiController.loopMainThreadUntilIdle()
        }
    }
}