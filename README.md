**What is this Repository All about?**

 - This repository will help you in fetching the Git Repositories of a specific Organisation from the Git public API "/repositories".
 - The application version is 1.0.0
 
 **How to setup?**

 - You need to have Android Studio installed on your machine.
 - Checkout the project from the repository bitbucket link.
 - This repository contains multiple dependencies of external libraries
 	- Retrofit for *Network Call*
	- Glide for *Image Loading*
	- Room for *Caching*
	- EventBus for *Event trigger*
 - Sync the project with all the above mentioned dependencies added to the project build.gradle file.
 - Install the build by clicking the run button of Android Studio.
 
 **Code Guideline & Architecture**
 
 - The project uses the MVVM + Repository Architechture.
 - The project structure have segregated layer of Data, UI and Utils.
 - SOLID Principle guidelines are followed.
 - Some of the Jetpack components are used in the project like ViewModel, LiveData, Room
 - The project also have offline support.


